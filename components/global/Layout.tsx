import React from 'react';
import styles from '../../styles/Home.module.css';
import Head from 'next/head';

interface Props {
    children: React.ReactNode;
    page: string;
}

const Layout: React.FC<Props> = (props: Props) => {
    return (
        <div className={styles.container}>
            <Head>
                <title>Quiz Interview - {props.page}</title>
                <meta name='description' content='Quick quiz for Holis interview' />
                <link rel='icon' href='/favicon.ico' />
            </Head>

            <main className={styles.main}>
                {props.children}
            </main>
        </div>
    );
};

export default Layout;
