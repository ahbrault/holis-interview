import React from 'react';

interface Props {
    children: React.ReactNode;
    classname?: string;
}

interface ItemProps extends Props {
    key: string;
    id: string;
    name?: string;
    value?: number;
    onChange?: (currentTarget: any) => void;
}

export const List: React.FC<Props> = (props: Props) => {
    return (
        <ul className={props.classname ? props.classname : ''}>
            {props.children}
        </ul>
    );
};

export const ListItem: React.FC<ItemProps> = (props: ItemProps) => {
    return (
        <li className={'flex ' + (props.classname ? props.classname : '')} key={props.key}>
            {props.onChange !== undefined ?
                <label className='flex-1 items-center rounded-full border border-gray-400 px-4 py-2 my-2'>
                    <input type='radio' className='form-checkbox' name={props.name} value={props.value}
                           onChange={props.onChange} id={props.id} />
                    <span className='ml-2'>{props.children}</span>
                </label>
                :
                props.children
            }
        </li>
    );
};
