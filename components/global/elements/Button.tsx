import React from 'react';

interface Props {
    children: React.ReactNode;
    onClick?: React.MouseEventHandler;
}

const Button: React.FC<Props> = (props: Props) => {
    return (
        <React.Fragment>
            <button
                className='mx-2 my-2 bg-indigo-700 transition duration-150 ease-in-out hover:bg-indigo-600 rounded text-white px-8 py-3 text-sm'
                onClick={props.onClick}>
                {props.children}
            </button>
        </React.Fragment>
    );
};

export default Button;
