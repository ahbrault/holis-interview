import React from 'react';

interface Props {
    className?: string;
    children: React.ReactNode;
}

export const Card: React.FC<Props> = (props: Props) => {
    return (
        <div
            className={'rounded-2xl bg-white border border-gray-50 shadow-md mb-6 w-96 mx-auto text-center ' + (props.className ? props.className : '')}>
            {props.children}
        </div>
    );
};

export const CardBody: React.FC<Props> = (props: Props) => {
    return (
        <div className={`p-5 ${props.className !== undefined && props.className}`}>
            {props.children}
        </div>
    );
};

export const CardTitle: React.FC<Props> = (props: Props) => {
    return (
        <div className='rounded-tl-2xl rounded-tr-2xl p-3 mb-2 bg-gradient-to-r from-green-400 to-blue-500'>
            <h3 className='text-xl text-center text-white uppercase tracking-wider'>
                {props.children}
            </h3>
        </div>
    );
};
