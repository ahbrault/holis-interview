import type { NextPage } from 'next';
import Layout from '../components/global/Layout';
import { List, ListItem } from '../components/global/elements/List';
import Button from '../components/global/elements/Button';
import { Card, CardBody, CardTitle } from '../components/global/elements/Card';
import React, { useState } from 'react';

interface Quiz {
    answers: Array<number>;
    correctAnswers: Array<number>;
}

const Home: NextPage = () => {
    const [quizzes, setQuizzes] = useState<Array<Quiz>>([
        { answers: [0, 0, 0, 0, 0], correctAnswers: [1, 2, 3, 4, 5] },
        { answers: [0, 0, 0, 0, 0], correctAnswers: [5, 4, 3, 2, 1] },
        { answers: [0, 0, 0, 0, 0], correctAnswers: [1, 2, 3, 4, 5] },
    ]);
    const [currentQuiz, setCurrentQuiz] = useState<number | undefined>();
    const [currentQuestion, setCurrentQuestion] = useState<number>(0);
    const [displayAnswers, setDisplayAnswers] = useState<boolean>(false);

    const handleChange = ({ currentTarget }) => {
        if (currentQuiz !== undefined) {
            const { value } = currentTarget;
            const tmpQuizzes = quizzes;
            const tmpAnswers = tmpQuizzes[currentQuiz].answers;
            tmpAnswers[currentQuestion] = parseInt(value);
            tmpQuizzes[currentQuiz].answers = tmpAnswers;
            setQuizzes(tmpQuizzes);
        } else {
            window.alert('Something went wrong. Please select a test');
        }
    };

    const getNextQuestion = () => {
        if (currentQuestion < 4)
            setCurrentQuestion(currentQuestion + 1);
        else
            setDisplayAnswers(true);
    };

    const resetQuiz = () => {
        setCurrentQuiz(undefined);
        setCurrentQuestion(0);
        setDisplayAnswers(false);
    };

    return (
        <Layout page='Home'>
            <div className='container mx-auto flex pt-6'>
                {currentQuiz === undefined ?
                    <div className='mx-auto text-center'>
                        <h1 className='mb-3'>Select a quiz</h1>
                        <Button onClick={() => setCurrentQuiz(0)}>
                            Quiz 1
                        </Button>
                        <Button onClick={() => setCurrentQuiz(1)}>
                            Quiz 2
                        </Button>
                        <Button onClick={() => setCurrentQuiz(2)}>
                            Quiz 3
                        </Button>
                    </div>
                    : !displayAnswers ? quizzes[currentQuiz].answers.map((question, index) => (
                            <Card className={currentQuestion === index ? '' : 'hidden'}
                                  key={`quiz_${currentQuiz}_${index}`}>
                                <CardTitle>
                                    Quiz {currentQuiz + 1} - Question {index + 1}
                                </CardTitle>
                                <CardBody>
                                    <List>
                                        {quizzes[currentQuiz].answers.map((answer, answerIndex) => (
                                            <ListItem name={'quiz' + currentQuiz + '_choices'} key={'answer_' + answerIndex}
                                                      value={answerIndex + 1}
                                                      onChange={handleChange} id={'quiz_' + (index + 1)}>
                                                {answerIndex + 1}
                                            </ListItem>
                                        ))}
                                    </List>
                                    <Button onClick={getNextQuestion}>Next Question</Button>
                                </CardBody>
                            </Card>
                        )) :
                        <Card>
                            <CardTitle>
                                Quiz {currentQuiz + 1} - Correction
                            </CardTitle>
                            <CardBody>
                                <List>
                                    {quizzes[currentQuiz].answers.map((answer, index) => (
                                        <ListItem id={'correctAnswer_' + index} key={'correctAnswer_' + index}
                                                  classname={'text-center ' + (answer !== quizzes[currentQuiz].correctAnswers[index] ? 'text-red-500' : 'text-green-500')}>
                                            {index + 1} : {answer} {
                                            answer !== quizzes[currentQuiz].correctAnswers[index] &&
                                            <span
                                                className=''>{' -> '} {quizzes[currentQuiz].correctAnswers[index]}</span>
                                        }
                                        </ListItem>
                                    ))}
                                </List>
                                <Button onClick={resetQuiz}>Finish</Button>
                            </CardBody>
                        </Card>
                }
            </div>
        </Layout>
    );
};

export default Home;
